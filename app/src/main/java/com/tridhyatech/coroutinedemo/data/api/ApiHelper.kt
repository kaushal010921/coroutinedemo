package com.tridhyatech.coroutinedemo.data.api

// Make a helper class that will be helpful to access ApiServices

class ApiHelper(private val apiService: ApiService) {

    suspend fun getUsers() = apiService.getUsers();
}