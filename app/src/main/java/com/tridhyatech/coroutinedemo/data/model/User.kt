package com.tridhyatech.coroutinedemo.data.model

// Make a data class for required fields

data class User(
    val avatar: String,
    val email: String,
    val id: String,
    val name: String
)