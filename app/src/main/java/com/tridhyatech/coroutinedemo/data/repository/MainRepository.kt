package com.tridhyatech.coroutinedemo.data.repository

import com.tridhyatech.coroutinedemo.data.api.ApiHelper

// Repository class for Accessing network class

class MainRepository(private val apiHelper: ApiHelper) {

//   Suspend helps the coroutine to suspend (pause),
//   perform the required job on a network thread (if Dispatchers.IO) is used,
//   wait for the response,
//   and then resumes from where it left off once the response is available.
    suspend fun getUsers() = apiHelper.getUsers()
}