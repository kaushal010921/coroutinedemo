package com.tridhyatech.coroutinedemo.data.api

import com.tridhyatech.coroutinedemo.data.model.User
import retrofit2.http.GET

// Make interface for Api services like GET,POST,PUT,DELETE etc.

interface ApiService {

    @GET("users")
    suspend fun getUsers(): List<User>
}