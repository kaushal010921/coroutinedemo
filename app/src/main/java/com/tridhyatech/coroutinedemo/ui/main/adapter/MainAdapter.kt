package com.tridhyatech.coroutinedemo.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tridhyatech.coroutinedemo.R
import com.tridhyatech.coroutinedemo.data.model.User
import kotlinx.android.synthetic.main.item_layout.view.*
import java.util.ArrayList

class MainAdapter(private val users: ArrayList<User>) : RecyclerView.Adapter<MainAdapter.DataViewHolder>(){

    class DataViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bind(user: User){
            itemView.apply {
                name.text = user.name
                Glide.with(image.context)
                    .load(user.avatar)
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(image)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder = DataViewHolder(LayoutInflater.from(parent.context).inflate(
        R.layout.item_layout, parent, false))

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(users[position])
    }

    override fun getItemCount(): Int = users.size

    fun addUsers(users: ArrayList<User>){
        this.users.apply {
            clear()
            addAll(users)
        }
    }
}