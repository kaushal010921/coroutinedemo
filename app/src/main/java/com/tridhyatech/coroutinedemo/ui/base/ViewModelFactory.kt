package com.tridhyatech.coroutinedemo.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tridhyatech.coroutinedemo.data.api.ApiHelper
import com.tridhyatech.coroutinedemo.data.repository.MainRepository
import com.tridhyatech.coroutinedemo.ui.main.viewmodel.MainViewModel

class ViewModelFactory(private var apiHelper: ApiHelper) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(MainViewModel::class.java))
            return MainViewModel(MainRepository(apiHelper)) as T
        throw IllegalArgumentException("Unknown class name")
    }
}