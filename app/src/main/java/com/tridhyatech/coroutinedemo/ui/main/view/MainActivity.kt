package com.tridhyatech.coroutinedemo.ui.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.google.android.material.snackbar.Snackbar
import com.tridhyatech.coroutinedemo.R
import com.tridhyatech.coroutinedemo.data.api.ApiHelper
import com.tridhyatech.coroutinedemo.data.api.RetrofitBuilder
import com.tridhyatech.coroutinedemo.data.model.User
import com.tridhyatech.coroutinedemo.ui.base.ViewModelFactory
import com.tridhyatech.coroutinedemo.ui.main.adapter.MainAdapter
import com.tridhyatech.coroutinedemo.ui.main.viewmodel.MainViewModel
import com.tridhyatech.coroutinedemo.utils.status.Status
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.internal.notifyAll
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpViewModel()
        setUpUI()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.getUsers().observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        recyclerView.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        resource.data?.let { it ->
                            retriveUsersList(it as ArrayList<User>)
                        }
                    }

                    Status.ERROR -> {
                        recyclerView.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                        it.message?.let { it1 -> Snackbar.make(mainRoot, it1, 3000).show() }
                    }

                    Status.LOADING -> {
                        recyclerView.visibility = View.GONE
                        progressBar.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun retriveUsersList(list: ArrayList<User>) {

        adapter.apply {
            addUsers(list)
            notifyAll()
        }

    }

    private fun setUpUI() {
        adapter = MainAdapter(arrayListOf())
        recyclerView.adapter = adapter
    }

    private fun setUpViewModel() {
        viewModel =
            ViewModelProvider(this, ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))).get(
                MainViewModel::class.java
            )
    }


}