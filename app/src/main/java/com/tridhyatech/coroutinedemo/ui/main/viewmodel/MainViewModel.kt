package com.tridhyatech.coroutinedemo.ui.main.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.tridhyatech.coroutinedemo.data.repository.MainRepository
import com.tridhyatech.coroutinedemo.utils.resource.Resource
import kotlinx.coroutines.Dispatchers

class MainViewModel(private val mainRepository: MainRepository) : ViewModel() {

//    Here, the getUsers function of the MainRepository class is a suspend function, and hence,
//    only once the network call(which is run on another thread, in this case, the thread from Dispatchers.IO) is completed (success or error),
//    the coroutine resumes by emitting the respective value that is obtained from the network call.

//    Here we have used liveData(Dispatchers.IO).
//    The result of the function will be emitted as Live Data, which can be observed in the view (Activity or Fragment).

    fun getUsers() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getUsers()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}