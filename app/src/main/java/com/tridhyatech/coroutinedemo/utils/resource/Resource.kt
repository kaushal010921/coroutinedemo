package com.tridhyatech.coroutinedemo.utils.resource

import com.tridhyatech.coroutinedemo.utils.status.Status

data class Resource<out T>(val status: Status, val data: T?, val message: String?) {

    companion object {
        fun <T> success(data: T): Resource<T> =
            Resource(status = Status.SUCCESS, data, message = null)

        fun <T> error(data: T, message: String?): Resource<T> =
            Resource(status = Status.ERROR, data, message = message)

        fun <T> loading(data: T): Resource<T> =
            Resource(status = Status.LOADING, data = data, message = null)
    }
}