package com.tridhyatech.coroutinedemo.utils.status

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}